#ifndef BME280_H_
#define BME280_H_
#include "stm32f0xx.h" 
#include <stdio.h>
#include <stdlib.h>
#include "I2C.h"

//0b1110110
#define i2c_address 0x76

//Requires I2C.h to work
typedef int32_t BME280_S32_t;
typedef uint32_t BME280_U32_t;

static BME280_S32_t t_fine;
//**variables for temperature trim values**
static uint16_t dig_T1;
static int16_t dig_T2,dig_T3;

//**variables for humidity trimming values
static uint8_t dig_H1,dig_H3;
static int16_t dig_H2,dig_H4,dig_H5;
static int8_t dig_H6;

//**variables for preassure trim values**
static uint16_t dig_P1;
static int16_t dig_P2,dig_P3,dig_P4,dig_P5,dig_P6,dig_P7,dig_P8,dig_P9;


void get_trimvalues(void);
void config_sensor(void);
int32_t get_temperature();
int16_t get_preassure();
uint16_t get_humidity();


//functions to get compensated values
BME280_S32_t BME280_compensate_T_int32(BME280_S32_t adc_T);
BME280_U32_t BME280_compensate_P_int64(BME280_S32_t adc_P);
BME280_U32_t bme280_compensate_H_int32(BME280_S32_t adc_H);

#endif /* BME280_H_ */
