#ifndef I2C_H_
#define I2C_H_
#include "stm32f0xx.h" 
#include <stdio.h>
#include <stdlib.h>
//basic I2C functions


void init_i2c(void);
void read_i2c(uint8_t device_address,uint8_t *data,uint8_t array_size, uint8_t start_address);
void write_i2c(uint8_t device_address, uint8_t reg_address,uint8_t data);

#endif /* I2C_H_ */
