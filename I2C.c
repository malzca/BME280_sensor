#include "I2C.h"


//init i2c bus
void init_i2c(void){
	
	//enable GPIOB clock
	RCC->AHBENR|=RCC_AHBENR_GPIOBEN;
	
	//enable I2C-clock
	RCC->APB1ENR|=RCC_APB1ENR_I2C1EN;
	
	//Set system clock as source of I2C
	RCC->CFGR3|=RCC_CFGR3_I2C1SW;
	
	//set GPIOB7 and GPIO8 for alternative mode 
	GPIOB->MODER|=GPIO_MODER_MODER7_1;
	GPIOB->MODER|=GPIO_MODER_MODER8_1;
	
	//set port speed for high speed
	GPIOB->OSPEEDR|=GPIO_OSPEEDR_OSPEEDR7;
	GPIOB->OSPEEDR|=GPIO_OSPEEDR_OSPEEDR8;
	
	//Set GPIO to work as I2C (alternative function 1) 
	GPIOB->AFR[0]|=GPIO_AFRL_AFRL1;
	GPIOB->AFR[1]|=GPIO_AFRH_AFRH1;
	
	//****Set timings for 100Khz***
	//Prescale 0b1011
	I2C1->TIMINGR|=(0xB<<28);
	
	//SCLL low time 5us
	I2C1->TIMINGR|=0x13;
	
	//SCLH High time 4us
	I2C1->TIMINGR|=(0xF<<8);
	
	//SDADEL 500ns
	I2C1->TIMINGR|=(0x2<<16);
	
	//SCLDEL 1250ns
	I2C1->TIMINGR|=(0x4<<20);
	
	//****END OF SET TIMING****
	
	//enable I2C peripheral
	I2C1->CR1|=I2C_CR1_PE;
	
	
}

//Read data from device (multiple or single)
void read_i2c(uint8_t device_address,uint8_t *data,uint8_t array_size, uint8_t start_address){
	uint8_t counter =0;
	
	//The transfer is not completed after the NBYTES data transfer
	I2C1->CR2|= I2C_CR2_RELOAD;
	
	//set device address
	I2C1->CR2|=(device_address<<16);
	
	//set I2C to write mode (to write register address)
	I2C1->CR2&=~(1<<10);
	
	
	//set NBYTES to 1 (1 byte transfer)
	I2C1->CR2|=(1<<16);
	
	if ((I2C1->ISR & I2C_ISR_TXE) == I2C_ISR_TXE)
	{			
		I2C1->TXDR = start_address;
		I2C1->CR2 |= I2C_CR2_START;
	}
	
	//loop while TXE not empty	
	while((I2C1->ISR & I2C_ISR_TXE) != I2C_ISR_TXE);
	
	//set I2C to read mode
	I2C1->CR2|=(1<<10);
	
	//set NBYTES to array_size (x byte transfer)
	I2C1->CR2|=(array_size<<16);
	
	//restart contidion
	I2C1->CR2 |= I2C_CR2_START;
	
	//loop until all data is received
	do{
		
		//loop until data is reseived
		while((I2C1->ISR & I2C_ISR_RXNE) != I2C_ISR_RXNE);
		data[counter]=I2C1->RXDR;
		counter++;
		
		
	}while(counter<=array_size);
	
}


//write one byte to specific register address
void write_i2c(uint8_t device_address, uint8_t reg_address,uint8_t data){
	
	//The transfer is not completed after the NBYTES data transfer
	I2C1->CR2|= I2C_CR2_RELOAD;
	
	//set NBYTES to 2 (2 byte transfer)
	I2C1->CR2|=(2<<16);
	
	//set device address
	I2C1->CR2|=(device_address<<16);
	
	//set I2C to write mode
	I2C1->CR2&=~(1<<10);
	
	
	//check that Tx is empty
	if ((I2C1->ISR & I2C_ISR_TXE) == I2C_ISR_TXE)
	{			
		I2C1->TXDR = reg_address;
		I2C1->CR2 |= I2C_CR2_START;
	}
		
	//loop while TXE not empty	
	while((I2C1->ISR & I2C_ISR_TXE) != I2C_ISR_TXE);
	
	//send data and send stop contidion
	I2C1->TXDR = data;
	I2C1->CR2|= I2C_CR2_STOP;
	while((I2C1->ISR & I2C_ISR_TXE) != I2C_ISR_TXE);
	
}