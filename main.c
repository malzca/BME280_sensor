#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "stm32f0xx.h"
#include "BME280.h"
#include "I2C.h"

/*
BME280 sensor
*/


void sys_clock_setup(void);


int main(void)
{
	uint32_t a,b;
	int32_t temp;
	sys_clock_setup();
	init_i2c();
	get_trimvalues();
	
	while(1){
		
		temp=get_temperature();
		for(a=0;a<0xffff;a++)
			for(b=0;b<0xff;b++);
	}
	
}

void sys_clock_setup(void){
	//PLL on, HSE on, PLLMUL=12,prediv=5, HSE as PLL source, system clock source=PLL. Crystal is 20Mhz so sysclk is 48Mhz
	//PLL MUST BE SETUP BEFORE TURNING ON
	
	//for the final version (or use with 20Mhz crystal)
	RCC->CFGR2|=RCC_CFGR2_PREDIV_DIV5;
	RCC->CFGR |=RCC_CFGR_PLLMUL12 | RCC_CFGR_PLLSRC | RCC_CFGR_SW_PLL;
	RCC->CR |=RCC_CR_PLLON | RCC_CR_HSEON;	
	
	//for prototyping (crystal is 8 Mhz so differend clock is needed)
	//RCC->CFGR |=RCC_CFGR_PLLMUL6 | RCC_CFGR_PLLSRC | RCC_CFGR_SW_PLL;
	//RCC->CR |=RCC_CR_PLLON | RCC_CR_HSEON;
	
	
}
