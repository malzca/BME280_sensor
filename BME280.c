#include "BME280.h"
#include "I2C.h"



//read trimvalues
void get_trimvalues(void){
	//calib26..calib41
	//uint8_t calibrate1[15];
	
	//calib00..calib25
	//uint8_t calibrate2[25];
	
	uint8_t calibrate_temp[6];
	//read_i2c(i2c_address,calibrate1,15,0xE1);
	read_i2c(i2c_address,calibrate_temp,6,0x88);
	
	dig_T1=(calibrate_temp[0] | (calibrate_temp[1]<<8));
	dig_T2=(calibrate_temp[2] | (calibrate_temp[3]<<8));
	dig_T3=(calibrate_temp[4] | (calibrate_temp[5]<<8));
	
	
}
	

void config_sensor(void){
	
	//t_sb=0.5ms, IIR=4, SPI=disable
	//write_i2c(i2c_address,0xF5,0x08);
	
}

//read and return temperature
int32_t get_temperature(){
	
	uint8_t temp[2];
	int32_t temperature;
	uint32_t a;
	

	//temp oversample 8x,forced measure (0b10010010)
	write_i2c(i2c_address,0xF4,0x92);
	
	//some delay
	for(a=0;a<0xFFFF;a++);
	
	
	read_i2c(i2c_address,temp,2,0xFA);
	
	temperature=(temp[1] | (temp[0]<<8));
	
	return BME280_compensate_T_int32(temperature);
}

int16_t get_preassure(){
	
}


uint16_t get_humidity(){
	
	
}


BME280_S32_t BME280_compensate_T_int32(BME280_S32_t adc_T){
	
	BME280_S32_t var1, var2, T;
	var1 = ((((adc_T>>3) - ((BME280_S32_t)dig_T1<<1))) * ((BME280_S32_t)dig_T2)) >> 11;
	var2 = (((((adc_T>>4) - ((BME280_S32_t)dig_T1)) * ((adc_T>>4) - ((BME280_S32_t)dig_T1))) >> 12) * ((BME280_S32_t)dig_T3)) >> 14;
	t_fine = var1 + var2;
	T = (t_fine * 5 + 128) >> 8;
	return T;

}

BME280_U32_t BME280_compensate_P_int64(BME280_S32_t adc_P){
	
	BME280_S32_t var1, var2;
	BME280_U32_t p;
	var1 = (((BME280_S32_t)t_fine)>>1) - (BME280_S32_t)64000;
	var2 = (((var1>>2) * (var1>>2)) >> 11 ) * ((BME280_S32_t)dig_P6);
	var2 = var2 + ((var1*((BME280_S32_t)dig_P5))<<1);
	var2 = (var2>>2)+(((BME280_S32_t)dig_P4)<<16);
	var1 = (((dig_P3 * (((var1>>2) * (var1>>2)) >> 13 )) >> 3) + ((((BME280_S32_t)dig_P2) * var1)>>1))>>18;
	var1 =((((32768+var1))*((BME280_S32_t)dig_P1))>>15);
	if (var1 == 0)
	{
		return 0; // avoid exception caused by division by zero
	}
	p = (((BME280_U32_t)(((BME280_S32_t)1048576)-adc_P)-(var2>>12)))*3125;
	if (p < 0x80000000)
	{
		p = (p << 1) / ((BME280_U32_t)var1);
	}
	else
	{
		p = (p / (BME280_U32_t)var1) * 2;
	}
	var1 = (((BME280_S32_t)dig_P9) * ((BME280_S32_t)(((p>>3) * (p>>3))>>13)))>>12;
	var2 = (((BME280_S32_t)(p>>2)) * ((BME280_S32_t)dig_P8))>>13;
	p = (BME280_U32_t)((BME280_S32_t)p + ((var1 + var2 + dig_P7) >> 4));
	return p;
	
}


BME280_U32_t bme280_compensate_H_int32(BME280_S32_t adc_H){
	
	BME280_S32_t v_x1_u32r;
	v_x1_u32r = (t_fine - ((BME280_S32_t)76800));
	v_x1_u32r = (((((adc_H << 14) - (((BME280_S32_t)dig_H4) << 20) - (((BME280_S32_t)dig_H5) * v_x1_u32r)) +
	((BME280_S32_t)16384)) >> 15) * (((((((v_x1_u32r * ((BME280_S32_t)dig_H6)) >> 10) * (((v_x1_u32r *
	((BME280_S32_t)dig_H3)) >> 11) + ((BME280_S32_t)32768))) >> 10) + ((BME280_S32_t)2097152)) *
	((BME280_S32_t)dig_H2) + 8192) >> 14));
	v_x1_u32r = (v_x1_u32r - (((((v_x1_u32r >> 15) * (v_x1_u32r >> 15)) >> 7) * ((BME280_S32_t)dig_H1)) >> 4));
	v_x1_u32r = (v_x1_u32r < 0 ? 0 : v_x1_u32r);
	v_x1_u32r = (v_x1_u32r > 419430400 ? 419430400 : v_x1_u32r);
	return (BME280_U32_t)(v_x1_u32r>>12);
	
}